package ee.bcs.valiit;

public class Test4_1 {
    public static int[] createArray(int a, int b, boolean asc) {

        int[] array = new int[2];
        if(asc == true) {
            array[0] = a;
            array[1] = b;
        }
        else {
            array[0] = b;
            array[1] = a;
        }

        return array;
    }

    public static void main(String[] args) {
        int[] array = createArray(2, 3, true);
        System.out.println(String.format("array: %d %d", array[0], array[1]));
        array = createArray(2, 3, false);
        System.out.println(String.format("array: %d %d", array[0], array[1]));
    }
}
