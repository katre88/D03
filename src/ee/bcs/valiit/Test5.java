package ee.bcs.valiit;

import java.util.Scanner;
public class Test5 {

    public static void main(String[] args) {
        String gender = deriveGender();
        System.out.println(gender);
    }

    public static String deriveGender() {

        Scanner ID_kood = new Scanner(System.in);
        System.out.println("Sisesta isikukood");
        String isikukood = ID_kood.nextLine();
        String sugu = isikukood.substring(0, 1);
        if((sugu.equals("2") || sugu.equals("4") || sugu.equals("6")) && isikukood.length() == 11) {
            return "F";
        }
        else{
            if((sugu.equals("1") || sugu.equals("3") || sugu.equals("5")) && isikukood.length() == 11) {
                return "M";

            }
            else {
                return "Midagi läks valesti.";
            }
        }


    }
}
