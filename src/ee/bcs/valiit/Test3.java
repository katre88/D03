package ee.bcs.valiit;


public class Test3 {

    public static final double VAT = 1.2;
    public static void main(String[] args) {
        System.out.println(String.format("Total: %s", addVat(10)));
    }

    public static double addVat(double a) {
        return a * VAT;


    }
}

