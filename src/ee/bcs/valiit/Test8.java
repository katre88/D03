package ee.bcs.valiit;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Test8 {

    public static boolean validatePersonalCode​() {

        Scanner ID_kood = new Scanner(System.in);
        System.out.println("Sisesta isikukood");
        BigInteger isikukood = new BigInteger(ID_kood.nextLine());
        if (isikukood.toString().length() == 11) {
            int[] ID_array = new int[11];
            for (int i = 0; i < isikukood.toString().length(); i++) {
                ID_array[i] = Integer.parseInt(isikukood.toString().substring(i, (i + 1)));
            }

            int[] ctrlArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
            int[] multArray = new int[10];
            for (int j = 0; j < ctrlArray.length; j++) {
                multArray[j] = ID_array[j] * ctrlArray[j];
            }

            int sum = IntStream.of(multArray).sum();

            int checkNr = sum % 11;
            String lastDig = isikukood.toString().substring(isikukood.toString().length() - 1);
            if (checkNr < 10 && Integer.toString(checkNr).equals(lastDig)) {
                return true;

            } else {
                int[] ID_array2 = new int[11];
                for (int i = 0; i < isikukood.toString().length(); i++) {
                    ID_array2[i] = Integer.parseInt(isikukood.toString().substring(i, (i + 1)));
                }

                int[] ctrlArray2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
                int[] multArray2 = new int[10];
                for (int j = 0; j < ctrlArray2.length; j++) {
                    multArray2[j] = ID_array2[j] * ctrlArray2[j];
                }

                int sum2 = IntStream.of(multArray2).sum();

                int checkNr2 = sum2 % 11;
                String lastDig2 = isikukood.toString().substring(isikukood.toString().length() - 1);
                if (checkNr2 < 10 && Integer.toString(checkNr2).equals(lastDig2)) {
                    return true;
                } else if (Integer.toString(0).equals(lastDig2)) {
                        return true;

                    } else {
                        return false;
                    }


                }
            } else{
                return false;
            }
        }

    public static void main(String[] args) {

        System.out.println(validatePersonalCode​());
    }


}
